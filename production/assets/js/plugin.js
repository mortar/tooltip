/* ----------------------------------------------------------------------------
URL: http://bitbucket.org/mortar/tooltip
Author: Daniel Charman (daniel@blackmaze.com.au)
Created: 2014-05-24
---------------------------------------------------------------------------- */
(function( $ ) {
 
    $.fn.tooltip = function(options) {

        var defaults = {   
            debug:      false,
            speed:      0,
            delay:      0,
            trigger:    'hover',
            position:   'bottom',
            template:   '<div class="m-tooltip" role="tooltip"><div class="m-tooltip-arrow"></div><div class="m-tooltip-inner"></div></div>'
        };
     
        var settings = $.extend( {}, defaults, options );

        this.each(function () {

            var self = $(this);

            var getTrigger = function(obj) {
                var trigger = settings.trigger;
                if(obj.data('trigger')) {
                    trigger = obj.data('trigger');
                }
                return trigger;
            }

            var getSpeed = function(obj) {
                var speed = settings.speed;
                if(obj.data('speed')) {
                    speed = obj.data('speed');
                }
                return speed;
            }

            var getDelay = function(obj) {
                var delay = settings.delay;
                if(obj.data('delay')) {
                    delay = obj.data('delay');
                }
                return delay;
            }

            var getPosition = function(obj) {
                var position = settings.position;
                if(obj.data('position')) {
                    position = obj.data('position');
                }
                return position;
            }

            var showTooltip = function(obj) {

                $('body').append(settings.template);

                var tooltipObj = $('body').find('.m-tooltip');

                tooltipObj.find('.m-tooltip-inner').html( obj.data('caption') );

                var position = getPosition(self);
                var delay = getDelay(self);
                var speed = getSpeed(self);

                var left = 0;
                var top = 0;

                switch(position) {
                    case 'left':
                        left = obj.offset().left - (tooltipObj.outerWidth() + 10);
                        top = obj.offset().top + ((obj.outerHeight() / 2) - (tooltipObj.outerHeight() / 2));

                        tooltipObj.addClass('left');
                    break;
                    case 'right':
                        left = (obj.offset().left + obj.outerWidth());
                        top = obj.offset().top + ((obj.outerHeight() / 2) - (tooltipObj.outerHeight() / 2));

                        tooltipObj.addClass('right');
                    break;
                    case 'top':
                        left = obj.offset().left + ((obj.outerWidth() / 2) - (tooltipObj.outerWidth() / 2));

                        top = obj.offset().top - (tooltipObj.outerHeight() + 10);

                        tooltipObj.addClass('top');
                    break;
                    case 'bottom':
                    default:
                        left = obj.offset().left + ((obj.outerWidth() / 2) - (tooltipObj.outerWidth() / 2));
                        top = obj.offset().top + obj.outerHeight();

                        tooltipObj.addClass('bottom');
                }

                tooltipObj.css({
                    'left':     left + 'px',
                    'top':      top + 'px'
                }).delay(delay).fadeIn(speed);
            }

            var hideTooltip = function(obj) {
                var delay = getDelay(self);
                var speed = getSpeed(self);

                $('body').find('.m-tooltip').delay(delay).fadeOut(speed, function() {
                    $(this).remove();
                });
            }

            var trigger = getTrigger($(this));

            if(trigger == 'click') {
                $(this).on('click', function(e) {
                    e.preventDefault();
                    if($('body .m-tooltip').length > 0) {
                        hideTooltip( $(this) );
                    }else{
                        $('.m-tooltip').remove();
                        showTooltip( $(this) );
                    }
                }); 
            }else{
                $(this).on('mouseover', function(e) {
                    e.preventDefault();
                    $('.m-tooltip').remove();
                    showTooltip( $(this) );
                });

                $(this).on('mouseout', function(e) {
                    e.preventDefault();
                    hideTooltip( $(this) );
                });                
            }
        });

        return this;
    };

}( jQuery ));