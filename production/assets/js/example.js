/* ----------------------------------------------------------------------------
URL: http://bitbucket.org/mortar/tooltip
Author: Daniel Charman (daniel@blackmaze.com.au)
Created: 2014-05-24
---------------------------------------------------------------------------- */
$('[data-tooltip]').tooltip({ 
	debug: 		true
});